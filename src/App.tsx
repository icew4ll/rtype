import React from 'react';
import './App.css';
import Katex from './components/Katex';

interface AppProps {}

function App({}: AppProps) {
  // Return the App component.
  return (
    <div className="App">
      <Katex />
    </div>
  );
}

export default App;
